#!/bin/bash
echo -e "Welcome Back!"

echo -e "Let's get started - Updating all the things."
#
# Update apt, and upgrade curently installed programs.
sudo apt update
sudo apt -y upgrade
#
# A few neccesities
echo "Installing necessities"
sudo apt install -y wget curl git openssh-server software-properties-common apt-transport-https ca-certificates gnupg-agent direnv
# Configure Git
echo "Setting up Git User"
git config --global user.name "Travis Reynolds"
git config --global user.email "travis@cirruscreative.co.uk"
#
# Now lets install ZSH & Oh-My-ZSH
echo "Installing ZSH + Oh-My-ZSH"
sudo apt -y install zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
#
# Download remote config files
echo "Getting config files..."
curl https://gitlab.com/travisreynolds/dotfiles/raw/master/cobalt2.zsh-theme > ~/.oh-my-zsh/themes/cobalt2.zsh-theme
curl https://gitlab.com/travisreynolds/dotfiles/raw/master/.zshrc > ~/.zshrc
#
# And we need Z - lifesaver
echo "Installing Z"
curl https://raw.githubusercontent.com/rupa/z/master/z.sh > ~/z.sh
touch ~/.z
#
# How about NVM?
echo "Installing NVM (and Node)"
zsh -c "$(curl -fsSL https://raw.githubusercontent.com/creationix/nvm/v0.35.3/install.sh)"
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
#
# And install Node
echo "Installing Node"
nvm install 12
nvm alias default 12
#
# Now, source the New Shell
echo "Sourcing ZSH"
source ~/.zshrc
#
# Docker, anyone?
echo "Adding Docker"
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu eoan stable"
sudo apt update
sudo apt install -y docker-ce docker-ce-cli containerd.io
sudo usermod -aG docker $USER
sudo systemctl enable docker
sudo curl -L "https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
#
# Tidy up!
echo "Cleaning up"
sudo apt update
sudo apt -y autoremove
sudo apt -y autoclean
