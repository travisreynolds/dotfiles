# dotfiles

A quick script to quickly setup a basic workspace.

Run this setup script with:

```bash
sh -c "$(curl -fsSL https://gitlab.com/travisreynolds/dotfiles/raw/master/start.sh)"
```

The WSL version with:

```bash
sh -c "$(curl -fsSL https://gitlab.com/travisreynolds/dotfiles/raw/master/wsl.sh)"
```

Or the Server version with:

```bash
sh -c "$(curl -fsSL https://gitlab.com/travisreynolds/dotfiles/raw/master/server.sh)"
```

There are a couple of things you may want to change, e.g. the Git user - [L40](https://gitlab.com/travisreynolds/dotfiles/blob/master/start.sh#L15)

To set ZSH as the default shell, you may need to run:

```bash
chsh -s $(which zsh)
```

Then logout and back in again.
