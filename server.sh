#!/bin/bash
echo -e "Welcome Back!"

echo -e "Let's get started - Updating all the things."
#
# Update apt, and upgrade curently installed programs.
sudo apt update
sudo apt -y upgrade
#
# A few neccesities
echo "Installing necessities"
sudo apt install -y wget curl git software-properties-common apt-transport-https ca-certificates

# Install Nala package manager
echo "Installing Nala"
sudo apt install -y nala

# Configure Git
echo "Setting up Git User"
git config --global user.name "Travis Reynolds"
git config --global user.email "travis@travisreynolds.dev"
git config --global init.defaultBranch main

#
# Docker, anyone?
if [ -x "$(command -v docker)" ]; then
    echo "Docker is already installed"

else
  echo "Adding Docker"

  # Add Docker's official GPG key:
  sudo apt-get update
  sudo apt-get install ca-certificates curl gnupg
  sudo install -m 0755 -d /etc/apt/keyrings
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
  sudo chmod a+r /etc/apt/keyrings/docker.gpg

  # Add the repository to Apt sources:
  echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
    $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

  sudo apt-get update

  sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
fi

#
# How about Volta?
echo "Installing Volta (and Node)"
curl https://get.volta.sh | bash

source /home/travis/.bashrc

volta install node@20
volta install pnpm
volta install yarn

# Direnv
sudo apt install direnv

#
# We love Fonts (copied from repo)
echo "Installing FiraCode"
sudo apt install fonts-firacode

#
# And we need Z - lifesaver
echo "Installing Z"
curl https://raw.githubusercontent.com/rupa/z/master/z.sh > /home/travis/z.sh
touch ~/.z
#
# Now lets install ZSH & Oh-My-ZSH
echo "Installing ZSH + Oh-My-ZSH"
echo "NOTE: If the scripts asks you if you want to change to ZSH now, hit no as we will do that later."
sudo apt -y install zsh
curl -sS https://starship.rs/install.sh | sh

#
# Download remote config files
echo "Getting config files..."
curl https://gitlab.com/travisreynolds/dotfiles/raw/master/cobalt2.zsh-theme > /home/travis/.oh-my-zsh/themes/cobalt2.zsh-theme
curl https://gitlab.com/travisreynolds/dotfiles/raw/master/.zshrc.server > /home/travis/.zshrc

#
# Tidy up!
echo "Cleaning up"
sudo apt update
sudo apt -y autoremove
sudo apt -y autoclean


#
# Now, source the New Shell
echo "Sourcing ZSH"
source /home/travis/.zshrc
chsh -s $(which zsh)