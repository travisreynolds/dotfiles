#!/bin/bash
echo -e "Welcome Back!"

echo -e "Let's get started - Updating all the things."
#
# Update apt, and upgrade curently installed programs.
sudo apt update
sudo apt -y upgrade
#
# A few neccesities
echo "Installing necessities"
sudo apt install -y wget curl git software-properties-common apt-transport-https ca-certificates gnupg-agent direnv unzip gzip xz-utils gnupg

# Install Nala package manager
echo "Installing Nala"
sudo apt install -y nala

# Configure Git
echo "Setting up Git User"
git config --global user.name "Travis Reynolds"
git config --global user.email "travis@travisreynolds.dev"
git config --global init.defaultBranch main
git config --global core.autocrlf false
git config --global core.eol lf

#
# Now lets install ZSH & Oh-My-ZSH
echo "Installing ZSH + Oh-My-ZSH"
sudo apt -y install zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
curl -sS https://starship.rs/install.sh | sh

#
# Download remote config files
echo "Getting config files..."
curl https://gitlab.com/travisreynolds/dotfiles/raw/master/cobalt2.zsh-theme > ~/.oh-my-zsh/themes/cobalt2.zsh-theme
curl https://gitlab.com/travisreynolds/dotfiles/raw/master/.zshrc.wsl > ~/.zshrc
#

# Docker
# Add Docker's official GPG key:
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc


# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update

sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin -y

sudo groupadd docker
sudo usermod -aG docker $USER
newgrp docker

# Prototools
curl -fsSL https://moonrepo.dev/install/proto.sh | bash 

proto install node 20
proto install bun latest
proto install pnpm latest
proto install yarn 1

#
# Doppler

# Add Doppler's GPG key
curl -sLf --retry 3 --tlsv1.2 --proto "=https" 'https://packages.doppler.com/public/cli/gpg.DE2A7741A397C129.key' | sudo apt-key add -

# Add Doppler's apt repo
echo "deb https://packages.doppler.com/public/cli/deb/debian any-version main" | sudo tee /etc/apt/sources.list.d/doppler-cli.list

# Fetch and install latest doppler cli
sudo apt-get update && sudo apt-get install -y doppler

#
# We love Fonts (copied from repo)
echo "Installing FiraCode"
sudo apt install fonts-firacode

#
# Tidy up!
echo "Cleaning up"
sudo apt update
sudo apt -y autoremove
sudo apt -y autoclean

#
# Now, source the New Shell
echo "Sourcing ZSH"
source ~/.zshrc
chsh -s $(which zsh)